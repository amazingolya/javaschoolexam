package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class CounterTest {

    @Test
    public void getEntries_shouldReturnEmptyListIfEmpty() {
        Counter<String> counter = new Counter<>();
        assertTrue(counter.getEntries().isEmpty());
    }

    @Test
    public void getEntries_shouldReturnSortedElements() {
        Counter<String> counter = new Counter<>();
        counter.add("aaa");
        counter.add("baa");
        counter.add("caa");
        counter.add("caa");
        counter.add("aaa");
        List<Counter.Entry<String>> list = counter.getEntries();
        assertTrue(list.get(0).getKey().equals("aaa"));
        assertTrue(list.get(1).getKey().equals("baa"));
        assertTrue(list.get(2).getKey().equals("caa"));
    }

    @Test
    public void getEntries_shouldReturnGroupedDuplicates() {
        Counter<String> counter = new Counter<>();
        counter.add("aaa");
        counter.add("baa");
        counter.add("caa");
        counter.add("caa");
        counter.add("aaa");
        List<Counter.Entry<String>> list = counter.getEntries();
        assertTrue(list.get(0).getKey().equals("aaa") && list.get(0).getCount() == 2);
        assertTrue(list.get(1).getKey().equals("baa") && list.get(1).getCount() == 1);
        assertTrue(list.get(2).getKey().equals("caa") && list.get(2).getCount() == 2);
        assertTrue(list.size() == 3);
    }
}
