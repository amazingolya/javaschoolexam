package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        final List<String> output = buildRPN(statement);
        return output == null ? null : calc(output);
    }

    private static List<String> buildRPN(String statement) {
        StringBuilder number = new StringBuilder();
        final List<String> output = new ArrayList<>();
        final Deque<Character> stack = new ArrayDeque<>();
        for (int i = 0; i < statement.length(); i++) {
            char current = statement.charAt(i);
            if (Character.isDigit(current) || current == '.') {
                number.append(current);
            } else {
                if (number.length() != 0) {
                    output.add(number.toString());
                    number = new StringBuilder();
                }
                if (current == '+' || current == '-') {
                    while (!stack.isEmpty() && (stack.peekFirst() == '+' || stack.peekFirst() == '-'
                            || stack.peekFirst() == '*' || stack.peekFirst() == '/')) {
                        output.add(stack.pop().toString());
                    }
                    stack.push(current);
                } else if (current == '*' || current == '/') {
                    while (!stack.isEmpty() && (stack.peekFirst() == '*' || stack.peekFirst() == '/')) {
                        output.add(stack.pop().toString());
                    }
                    stack.push(current);
                } else if (current == '(') {
                    stack.push(current);
                } else if (current == ')') {
                    while (stack.peekFirst() != '(') {
                        output.add(stack.pop().toString());
                    }
                    stack.pop();
                } else {
                    return null;
                }
            }
        }
        if (number.length() != 0) {
            output.add(number.toString());
        }
        while (!stack.isEmpty()) {
            output.add(stack.pop().toString());
        }
        return output;
    }

    private static String calc(List<String> rpn) {
        final Deque<Float> stack = new ArrayDeque<>();
        for (String element : rpn) {
            switch (element) {
                case "+":
                    if (stack.size() <= 1) {
                        return null;
                    }
                    float last = stack.pop();
                    float first = stack.pop();
                    stack.push(first + last);
                    break;
                case "-":
                    if (stack.size() <= 1) {
                        return null;
                    }
                    last = stack.pop();
                    first = stack.pop();
                    stack.push(first - last);
                    break;
                case "*":
                    if (stack.size() <= 1) {
                        return null;
                    }
                    last = stack.pop();
                    first = stack.pop();
                    stack.push(first * last);
                    break;
                case "/":
                    if (stack.size() <= 1) {
                        return null;
                    }
                    last = stack.pop();
                    first = stack.pop();
                    if (last == 0) {
                        return null;
                    }
                    stack.push(first / last);
                    break;
                default:
                    try {
                        stack.push(Float.parseFloat(element));
                    } catch (NumberFormatException e) {
                        return null;
                    }
            }
        }

        final NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        numberFormat.setMaximumFractionDigits(4);
        return numberFormat.format(stack.pop());
    }

}
