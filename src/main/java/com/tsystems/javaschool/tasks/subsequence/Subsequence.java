package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (Objects.isNull(x) || Objects.isNull(y)) {
            throw new IllegalArgumentException("Lists cannot be null!");
        }

        int current = 0;
        for (int i = 0; i < x.size(); i++) {
            current = indexOf(y, x.get(i), current);
            if (current == -1) {
                return false;
            }
        }
        return true;
    }

    private static <T> int indexOf(List<T> y, T element, int startPosition) {
        for (int i = startPosition; i < y.size(); i++) {
            if ((Objects.equals(y.get(i), element))) {
                return i;
            }
        }
        return -1;
    }
}
