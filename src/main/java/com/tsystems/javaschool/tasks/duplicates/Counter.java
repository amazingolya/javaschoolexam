package com.tsystems.javaschool.tasks.duplicates;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class Counter<T extends Comparable<T>> {
    private final TreeMap<T, Integer> map = new TreeMap<>();

    public void add(T element) {
        final Integer count = map.get(element);
        if (count != null) {
            map.put(element, count + 1);
        } else {
            map.put(element, 1);
        }
    }

    public List<Entry<T>> getEntries() {
        final List<Entry<T>> list = new ArrayList<>(map.size());
        T currentKey = map.isEmpty() ? null : map.firstKey();
        while (currentKey != null) {
            list.add(new Entry<>(currentKey, map.get(currentKey)));
            currentKey = map.higherKey(currentKey);
        }
        return list;
    }

    public static class Entry<T extends Comparable<T>> {
        private final T key;
        private final int count;

        public Entry(T key, int count) {
            this.key = key;
            this.count = count;
        }

        public T getKey() {
            return key;
        }

        public int getCount() {
            return count;
        }
    }
}
