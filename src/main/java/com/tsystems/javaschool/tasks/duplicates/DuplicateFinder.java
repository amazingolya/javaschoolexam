package com.tsystems.javaschool.tasks.duplicates;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Stream;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException("Source and target files cannot be null!");
        }
        final Counter<String> counter = new Counter<>();
        try (Stream<String> stream = Files.lines(sourceFile.toPath())) {
            stream.forEach(str -> counter.add(str));
            writeToFile(counter, targetFile);
        } catch (IOException e) {
            return false;
        }
        return true;
    }


    private static void writeToFile(Counter<String> output, File out) throws IOException {
        if (!out.exists()) {
            out.createNewFile();
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(out, true))) {
            for (Counter.Entry<String> entry : output.getEntries()) {
                bufferedWriter.write(entry.getKey() + "[" + entry.getCount() + "]");
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        }
    }
}
